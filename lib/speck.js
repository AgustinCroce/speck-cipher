"use strict";

const {xor, shiftRigth, shiftLeft, modularAdd, modularSubstract} = require("./utils");
const R = require("rambda");

const speckCipherFirstFeistel = R.curry((alpha, key, [firstWord, secondWord]) => {
    const feistel = R.compose(xor(key), modularAdd(secondWord), shiftRigth(alpha));
    return [secondWord, feistel(firstWord)];
});

const speckCipherSecondFeistel = R.curry((beta, [firstWord, secondWord]) => {
    const feistel = R.compose(xor(secondWord), shiftLeft(beta));
    return [secondWord, feistel(firstWord)];
})

const speckCipher = R.curry((alpha, beta, key, words) => {
    return R.compose(speckCipherSecondFeistel(beta), speckCipherFirstFeistel(alpha, key))(words);
});

const speckDecipherFirstFeistel = R.curry((beta, [firstWord, secondWord]) => {
    const feistel = R.compose(shiftRigth(beta), xor(firstWord));
    return [firstWord, feistel(secondWord)];
});

const speckDecipherSecondFeistel = R.curry((alpha, key, [firstWord, secondWord]) => {
    const feistel = R.compose(shiftLeft(alpha), modularSubstract(secondWord), xor(key));
    return [feistel(firstWord), secondWord];
});

const speckDecipher = R.curry((alpha, beta, key, [firstWord, secondWord]) => {
    return R.compose(speckDecipherSecondFeistel(alpha, key), speckDecipherFirstFeistel(beta))(words);
});