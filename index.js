"use strict";

function shiftLeft(number, word) {
    return word.map((byte, index) => {
        const nextByteIndex = (index + 1) % word.length;
        const nextByte = word[nextByteIndex];

        const mask = ((2 ** (8 - number)) - 1);
        const inverseMask = mask ^ (2 ** 8 - 1);

        return (byte & mask) << number | (nextByte & inverseMask) >> (8 - number);
    });
}

function shiftRigth(number, word) {
    return word.map((byte, index) => {
        const previousByteIndex = (index + (word.length - 1)) % word.length;
        const previousByte = word[previousByteIndex];

        if(number === 8 ){
            return previousByte;
        }

        const mask = (2 ** number - 1) << (8 - number);
        const inverseMask = mask ^ (2 ** 8 - 1);

        return (byte & mask) >> (8 - number) | (previousByte & inverseMask) << number;
    });
}

function modularAdd(firstWord, secondWord) {
    let carry = 0;
    const firstWordInt32 = new Uint32Array(firstWord);
    const secondWordInt32 = new Uint32Array(secondWord);
    
    const result = firstWordInt32.slice().reverse().map((byte, index) => {
        const sum = carry + byte + secondWordInt32.slice().reverse()[index]; 
        carry = 0;
        
        if ( sum >= 256 ) {            
            carry = 1;
            return sum - 256;
        }

        return sum;
    });

    return new Uint8Array(result.reverse());
}

function modularSubstract(firstWord, secondWord) {
    let carry = 0;
    const firstWordInt32 = new Uint32Array(firstWord);
    const secondWordInt32 = new Uint32Array(secondWord);
    
    const result = firstWordInt32.slice().reverse().map((byte1, index) => {
        const byte2 = secondWordInt32.slice().reverse()[index];
    
        if (byte1 >= byte2 + carry) {
            const result = byte1 - byte2 - carry; 
            carry = 0;
            return result;
        }

        const result = 256 + byte1 - byte2 - carry;
        carry = 1
        return result;
    });
    
    return new Uint8Array(result.reverse());
}

function xor(firstWord, secondWord) {
    return firstWord.map((byte, index) => byte ^ secondWord[index]);
}

function createArray(hex) {
    return new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
    }));
}

function speckFirstFeistelCipher(alpha, key, [firstWord, secondWord]) {
    return [secondWord, xor(modularAdd(shiftRigth(firstWord, alpha), secondWord), key)]
}

function speckSecondFeistelCipher(beta, [firstWord, secondWord]) {
    return [secondWord, xor(shiftLeft(firstWord, beta), secondWord)]
}

console.log(modularSubstract(createArray("0000"), createArray("0009")));

module.exports = {
    createArray,
    shiftLeft,
    shiftRigth,
    modularAdd
};